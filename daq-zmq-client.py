# coding=utf-8
import zmq, yaml, datetime, sys
import struct
from unpacker import unpacker
from rpi_data_tests import checker  # for data tests
from optparse import OptionParser
from pathlib import Path
import paramiko


class yaml_config:
    yaml_opt = yaml.YAMLObject()

    def __init__(self, finame):
        self.yaml_opt = yaml.safe_load(open(finame))

    def dump(self):
        # print('Conf-string:')
        # print(yaml.dump(self.default_options))
        return yaml.dump(self.yaml_opt)

    def dumpToYaml(self, fname):  # writing to YAML files
        with open(fname, 'w') as fout:
            yaml.dump(self.yaml_opt, fout)


def get_comma_separated_args(option, opt, value, prser):
    setattr(prser.values, option.dest, value.split(','))


'''-----------Define Configuration---------'''
# setup parser options
parser = OptionParser()
parser.add_option('-g', '--noGui', dest="guiOFF", action="store_true",
                  help="set to true if you don't want to use the gui")
parser.add_option('-e', '--dataNotSaved', dest="dataNotSaved", action="store_true",
                  help="set to true if you don't want to save the data (and the yaml file)")
parser.add_option("-a", "--externalChargeInjection", dest="externalChargeInjection", action="store_true",
                  help="set to use external injection")
choices_m = ["standard", "sweep", "fixed", "const_inj", "instrumental_trigger", "external_trigger"]
parser.add_option("-b", "--acquisitionType", dest="acquisitionType", choices=choices_m,
                  help="acquisition method, valid choices are:\t%s" % (choices_m))
parser.add_option('-c', '--channelIds', dest="channelIds", action="callback", type=str,
                  help="channel Ids for charge injection", callback=get_comma_separated_args)
parser.add_option('-i', '--configfile', dest="conffile", type=str,
                  help="input for configurations file")
parser.add_option('-d', '--injectionDAC', dest="injectionDAC", type="int", action="store",
                  help="DAC setting for injection when acquisitionType is const_inj")
parser.add_option("-f", "--pulseDelay", dest="pulseDelay", type="int", action="store",
                  help="pulse delay (arbitrary unit) w.r.t. the trigger")

(options, args) = parser.parse_args()
print('parseroptions:', options)

# initialize config file
filename = 'default_config.yaml'  # changeable by parser
if options.conffile is not None:
    filename = options.conffile
config = yaml_config(filename)

# shortcuts
daq_options = config.yaml_opt['daq_options']
glb_options = config.yaml_opt['glb_options']
store_options = config.yaml_opt['store_options']
testswitches = config.yaml_opt['testswitches']
iv_variables = config.yaml_opt['iv_variables']

# update config with parser options, only if something entered
if options.guiOFF is not None:
    (glb_options['guiON']) = not options.guiOFF
if options.dataNotSaved is not None:
    (store_options['dataNotSaved']) = options.dataNotSaved
if options.externalChargeInjection is not None:
    (daq_options['externalChargeInjection']) = options.externalChargeInjection
if options.acquisitionType is not None:
    (daq_options['acquisitionType']) = options.acquisitionType
if options.channelIds is not None:
    (daq_options['channelIds']) = [options.channelIds]
if options.injectionDAC is not None:
    (daq_options['injectionDAC']) = options.injectionDAC
if options.pulseDelay is not None:
    (daq_options['pulseDelay']) = options.pulseDelay

"""if daq_options['gpibON'] is True:
    import IV_curve
    import matplotlib.pyplot as plt
    from pymeasure.instruments.keithley import Keithley2400
    from time import sleep"""

'''-------Overall run-command-------'''


def run_test():
    # create folders and files
    the_time = datetime.datetime.now()
    ivPath = store_options['outputCommonPath'] + glb_options['type_of_hardware'] + str(
        glb_options['DUTNumber']) + '/IV_LOG/'
    rawPath = store_options['outputCommonPath'] + glb_options['type_of_hardware'] + str(
        glb_options['DUTNumber']) + '/RawFiles/'
    configPath = store_options['outputCommonPath'] + glb_options['type_of_hardware'] + str(
        glb_options['DUTNumber']) + '/ConfigOut/'
    txtPath = store_options['outputCommonPath'] + glb_options['type_of_hardware'] + str(
        glb_options['DUTNumber']) + '/txt_data/'
    Name = glb_options['type_of_hardware'] + str(glb_options['DUTNumber']) + '_' + str(the_time.year) + "-" + str(
        the_time.month) + "-" + str(
        the_time.day) + "_" + str(the_time.hour) + "-" + str(the_time.minute) + "-" + str(the_time.second)
    Path(ivPath).mkdir(parents=True, exist_ok=True)
    Path(rawPath).mkdir(parents=True, exist_ok=True)
    Path(configPath).mkdir(parents=True, exist_ok=True)
    Path(txtPath).mkdir(parents=True, exist_ok=True)

    # IV-Curve (not yet implemented)
    '''if daq_options['gpibON'] is True:
        if testswitches['IV_curve'] is True:
            (voltArray, currArray) = IV_curve.IVcurve(iv_variables['gpib_adr'], iv_variables['vMax'],
                                                      iv_variables['compCurr'], iv_variables['stepSize'],
                                                      iv_variables['delay'])
            # gpib_adr, vMax, complianceCurrent, stepSize, delay_between_steps
            plt.figure(1)
            plt.xlabel('Voltage (V)')
            plt.ylabel('Current (mA)')
            plt.title('Module ' + str(glb_options['DUTNumber']))
            plt.plot(voltArray, currArray)
            data_folder = Path(ivPath) / (Name + ".pdf")
            if not glb_options['dataNotSaved']:
                plt.savefig(str(data_folder))  # Save a plot as pdf
            # plt.show()
            plt.close(1)
            data_folder = Path(ivPath) / (Name + ".csv")
            csvfile = data_folder.open(mode='w')  # save IV data as csv
            firstrow = 'Date:' + str(the_time.day) + "-" + str(the_time.month) + "-" + str(
                the_time.year) + "_Time:" + str(the_time.hour) + "-" + str(the_time.minute) + "-" + str(the_time.second)
            secondrow = 'Sensor_Number:' + str(glb_options['DUTNumber'])
            thirdrow = 'vMAX=' + str(iv_variables['vMax']) + '-' + 'compcurrent=' + str(
                iv_variables['compCurr']) + '-' + 'stepsize=' + str(iv_variables['stepSize']) + '-' + 'delay=' + str(
                iv_variables['delay'])
            fourthrow = 'temp:' + iv_variables['temperature'] + '_humidity:' + iv_variables['humidity']
            if not glb_options['dataNotSaved']:
                csvfile.writelines(
                    [str(firstrow) + '\n', str(secondrow) + '\n', str(thirdrow) + '\n', str(fourthrow) + '\n'])
                for i in range(len(voltArray)):
                    csvfile.write(str(voltArray[i]) + ',' + str(currArray[i]) + '\n')
            csvfile.close()'''

    # Connection to the RPI
    # in Python3, all strings are represented in unicode -> socket.send works just for bytes.
    # Use socket.send_string instead! (http://pyzmq.readthedocs.io/en/latest/unicode.html#unicode)

    '''if daq_options['gpibON'] is True:
        # Set HV
        keith_str = "GPIB::" + str(iv_variables['gpib_adr'])
        keith = Keithley2400(keith_str)
        keith.reset()'''
    if int(daq_options['nEvent']) > 0:
        '''if daq_options['gpibON'] is True:
            keith.use_rear_terminals()
            keith.apply_voltage(None, iv_variables['compCurr'])
            keith.enable_source()'''
        if glb_options['startServerManually'] is False:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            k = paramiko.RSAKey.from_private_key_file(glb_options['sshKeyPi'])
            print('Trying to connect to RPi...')
            ssh.connect(glb_options['serverIpAdress'], 22, username=glb_options['userName'], pkey=k)
            print('...successful')
            stdin, stdout, stderr = ssh.exec_command(
                "nohup python " + glb_options['serverCodePath'] + "/daq-zmq-server.py > log.log 2>&1&")
            ssh.close()
        context = zmq.Context()
        socket = context.socket(zmq.REQ)
        print("Send request to server")
        socket.connect("tcp://" + glb_options['serverIpAdress'] + ":5555")
        cmd = "DAQ_CONFIG"
        print(cmd)
        socket.send_string(cmd)
        status = socket.recv_string()
        print(status)
        if status == "READY_FOR_CONFIG":
            socket.send_string(config.dump())
            the_config = socket.recv_string()
            print("Returned DAQ_CONFIG:\n%s" % the_config)
        else:
            print("WRONG STATUS -> exit()", status)
            exit()

        # prepare for unpacking
        dataSize = 30786  # 30784 + 2 for injection value

        if daq_options['compressRawData'] is True:
            dataSize = 15394  # 30784/2 + 2 for injection value
        dataStringUnpacker = struct.Struct('B' * dataSize)

        # Test Configuration String (Currently not working)
        '''the_bit_string=sk2conf.bit_string()
        if daq_options['externalChargeInjection']==True:
            the_bit_string.set_channels_for_charge_injection(daq_options['channelIds'])
        if daq_options['preampFeedbackCapacitance']>63:
            print("!!!!!!!!! WARNING :: preampFeedbackCapacitance should not be higher than 63 !!!!!!!")
        the_bit_string.set_preamp_feedback_capacitance(daq_options['preampFeedbackCapacitance'])
        the_bit_string.set_channels_to_mask(daq_options['channelIdsToMask'])
        the_bit_string.set_channels_to_disable_trigger_tot(daq_options['channelIdsDisableTOT'])
        the_bit_string.set_channels_to_disable_trigger_toa(daq_options['channelIdsDisableTOA'])
        the_bit_string.set_lg_shaping_time(daq_options['shapingTime'])
        the_bit_string.set_hg_shaping_time(daq_options['shapingTime'])
        the_bit_string.set_tot_dac_threshold(daq_options['totDACThreshold'])
        a=the_bit_string.get_48_unsigned_char_p()
        #the_bit_string.Print()
        print( "outputBitString", [hex(a[i]) for i in range(len(a))] )'''

        # Read Events and Online Data-analysis
        for hv in [daq_options['hv']]:  # loop over all voltage steps
            outputFile = None
            if not store_options['dataNotSaved']:
                data_folder = Path(rawPath) / (Name + ".raw")
                print("open output file : ", data_folder)
                outputFile = open(data_folder, 'wb')
                if store_options['storeYamlFile']:
                    data_folder = Path(configPath) / (Name + ".yaml")
                    print("save yaml file : ", data_folder)
                    config.dumpToYaml(data_folder)
            cmd = "CONFIGURE"
            print(cmd)
            socket.send_string(cmd)
            return_bitstring = socket.recv_string()
            print("Returned bit string = ", return_bitstring)
            bitstring = [int(i, 16) for i in return_bitstring.split()]
            print("\t write bits string in output file")
            byteArray = bytearray(bitstring)
            if not store_options['dataNotSaved']:
                outputFile.write(byteArray)
            cmd = "PROCESS_AND_PUSH_N_EVENTS"
            socket.send_string(cmd)
            mes = socket.recv_string()
            print('\t***', mes, '***')
            '''if daq_options['gpibON'] is True:
                keith.ramp_to_voltage(hv)
                keith.measure_current(1, iv_variables['compCurr'], True)
                sleep(5)'''
            puller = context.socket(zmq.PULL)
            puller.connect("tcp://" + glb_options['serverIpAdress'] + ":5556")
            try:
                while True:
                    for i in range(int(daq_options['nEvent'])):  # loop over all events
                        daq_options['currEvent'] = i
                        str_data = puller.recv()
                        rawdata = dataStringUnpacker.unpack(str_data)
                        print("\n \n \n Receive event ", i)

                        # unpacking...
                        byteArray = bytearray(rawdata)
                        up = unpacker(config)
                        up.unpack(byteArray)
                        if store_options['showFullData']:
                            up.showData(i,) 
                        if store_options['printFullData']:
                            up.printData(i, txtPath, Name)

                        # Online Data- Tests
                        data_to_check = checker(up.sk2cms_data, up.rollMask, config, Name, txtPath)
                        if testswitches['RollMask_full_ON']:
                            data_to_check.check_full_RollMask()
                        if testswitches['SCA_full_ON']:
                            data_to_check.check_full_sca()
                        if testswitches['ToT_ToA_full_ON']:
                            data_to_check.check_full_TOA_TOT()
                        if store_options['printUnusualData']:
                            data_to_check.printUnusualData()
                        if store_options['showUnusualData']:
                            data_to_check.showUnusualData()

                        # print .raw file
                        if not store_options['dataNotSaved']:
                            outputFile.write(byteArray)
                    print('Loop over events finished')
                    break


            except KeyboardInterrupt:
                print("keyboard interruption")
                '''if daq_options['gpibON'] is True:
                    keith.shutdown()'''
                ssh = paramiko.SSHClient()
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                k = paramiko.RSAKey.from_private_key_file('pclcd35')
                ssh.connect(glb_options['serverIpAdress'], 22, username=glb_options['userName'], pkey=k)
                stdin, stdout, stderr = ssh.exec_command("killall python")
                ssh.close()
            puller.close()
        socket.send_string("END_OF_RUN")
        if socket.recv_string() == "CLOSING_SERVER":
            print("CLOSING SERVER")
            socket.close()
            context.term()
    '''if daq_options['gpibON'] is True:
        keith.shutdown()'''


'''-----------Class for GUI - APP---------'''
if __name__ == "__main__":
    if glb_options['guiON']:
        import client_GUI
        from PyQt5 import QtWidgets
        app = QtWidgets.QApplication(sys.argv)
        window = client_GUI.Select_and_launch_testsApp(run_test, config)
        window.show()
        sys.exit(app.exec_())
    else:
        run_test()
