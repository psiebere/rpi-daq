from PyQt5 import QtCore, QtGui, QtWidgets, uic

qtCreatorFile = "select_and_launch_tests.ui"
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class Select_and_launch_testsApp(QtWidgets.QMainWindow, Ui_MainWindow):
    config = None
    my_run = None

    def __init__(self, runtest, config):
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.btnLAUNCH.clicked.connect(self.launch_tests)
        self.config = config
        self.my_run = runtest
        # Set Inputs to value from Config
        self.ev.setText(str(config.yaml_opt['daq_options']['nEvent']))
        self.acqtype.setText(config.yaml_opt['daq_options']['acquisitionType'])
        self.inputch.setText(str(*config.yaml_opt['daq_options']['channelIds']))
        self.injDAC.setText(str(config.yaml_opt['daq_options']['injectionDAC']))
        self.delay.setText(str(config.yaml_opt['daq_options']['pulseDelay']))
        self.DUT.setText(str(config.yaml_opt['glb_options']['DUTNumber']))
        self.Hardwaretype.setText(str(config.yaml_opt['glb_options']['type_of_hardware']))
        self.Manufacturer.setText(str(config.yaml_opt['glb_options']['manufacturer']))
        self.gpib.setText(str(config.yaml_opt['iv_variables']['gpib_adr']))
        self.vmax.setText(str(config.yaml_opt['iv_variables']['vMax']))
        self.compcurr.setText(str(config.yaml_opt['iv_variables']['compCurr']))
        self.stepsize.setText(str(config.yaml_opt['iv_variables']['stepSize']))
        self.ivdelay.setText(str(config.yaml_opt['iv_variables']['delay']))
        self.hv.setText(str(config.yaml_opt['daq_options']['hv']))
        self.btnRM.setChecked(bool(self.config.yaml_opt['testswitches']['RollMask_full_ON']))
        self.btnTO.setChecked(bool(self.config.yaml_opt['testswitches']['ToT_ToA_full_ON']))
        self.btnSCA.setChecked(bool(self.config.yaml_opt['testswitches']['SCA_full_ON']))
        self.btnIV.setChecked(bool(self.config.yaml_opt['testswitches']['IV_curve']))
        self.btnINJ.setChecked(bool(self.config.yaml_opt['daq_options']['externalChargeInjection']))


    def launch_tests(self):
        # Update Configuration
        self.config.yaml_opt['daq_options']['nEvent'] = int(self.ev.toPlainText())
        self.config.yaml_opt['daq_options']['acquisitionType'] = self.acqtype.toPlainText()
        self.config.yaml_opt['daq_options']['injectionDAC'] = int(self.injDAC.toPlainText())
        self.config.yaml_opt['daq_options']['pulseDelay'] = int(self.delay.toPlainText())
        self.config.yaml_opt['glb_options']['DUTNumber'] = self.DUT.toPlainText()
        self.config.yaml_opt['glb_options']['type_of_hardware'] = self.Hardwaretype.toPlainText()
        self.config.yaml_opt['glb_options']['manufacturer'] = self.Manufacturer.toPlainText()
        self.config.yaml_opt['iv_variables']['gpib_adr'] = int(self.gpib.toPlainText())
        self.config.yaml_opt['iv_variables']['vMax'] = int(self.vmax.toPlainText())
        self.config.yaml_opt['iv_variables']['compCurr'] = float(self.compcurr.toPlainText())
        self.config.yaml_opt['iv_variables']['stepSize'] = int(self.stepsize.toPlainText())
        self.config.yaml_opt['iv_variables']['delay'] = float(self.ivdelay.toPlainText())
        channels = []
        if self.inputch.toPlainText() == 'all':
            for i in range(64):
                channels.append(int(i))
            self.config.yaml_opt['daq_options']['channelIds'] = channels
        elif self.inputch.toPlainText() in ['',' ','[]','[ ]']:
            self.config.yaml_opt['daq_options']['channelIds'] = []
        else:
            channels = self.inputch.toPlainText().split(',')
            channels = [int(i) for i in channels]
            self.config.yaml_opt['daq_options']['channelIds'] = channels

        if self.hv.toPlainText() == ('' or ' '):
            self.config.yaml_opt['daq_options']['hv'] = [int(0)]
        else:
            hv_values = self.hv.toPlainText().split(',')
            hv_values = [int(i) for i in hv_values]
            print("hv", hv_values)
            self.config.yaml_opt['daq_options']['hv'] = hv_values
        self.config.yaml_opt['testswitches']['RollMask_full_ON'] = self.btnRM.isChecked()
        self.config.yaml_opt['testswitches']['ToT_ToA_full_ON'] = self.btnTO.isChecked()
        self.config.yaml_opt['testswitches']['SCA_full_ON'] = self.btnSCA.isChecked()
        self.config.yaml_opt['testswitches']['IV_curve'] = self.btnIV.isChecked()
        self.config.yaml_opt['daq_options']['externalChargeInjection'] = self.btnINJ.isChecked()

        # Perform tests
        if not bool(self.config.yaml_opt['daq_options']['externalChargeInjection']):
            self.my_run()
        else:
            for i in channels:
                self.config.yaml_opt['daq_options']['channelIds'] = [i]
                self.my_run()
