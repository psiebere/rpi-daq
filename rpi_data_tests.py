# Class for online and offline rpi data tests
import yaml
from pathlib import Path

set_rm = {3, 6, 12, 24, 48, 96, 192, 384, 768, 1536, 3072, 6144, 4097}  # all possible values for rollMask

# FOR ALL TESTS: SCA=SwitchCapacitorArray (0-14), SK=SkiROC (0-3), ch=Channel(0-127), i=Event number

class checker():
    data_array = []
    rollMask = []
    curr_ev = 0
    conf = yaml.YAMLObject()
    data_folder_short = None
    outputfile_short = None

    def __init__(self, da, rm, conf, name, txtPath):
        self.data_array = da  # Style: [[0 for sca in range(15)] for ch in range(64)] for sk in range(4)]
        self.rollMask = rm  # Style: =[0x0000 for sk in range(4)]
        self.conf = conf
        self.curr_ev = self.conf.yaml_opt['daq_options']['currEvent']
        self.data_folder_short = Path(txtPath) / (name + '_short' + ".txt")  # write short .txt file

    def showUnusualData(self):
        print('*********** Unusual-Data *************\n')
        for sk in self.conf.yaml_opt['testswitches']['chip_array']:
            print("Event = " + str(self.curr_ev) + "\t Chip = " + str(sk) + "\t RollMask = " + str(
                    hex(self.rollMask[sk])) + '\n')
            if self.check_RollMask(sk) is True:
                self.outputfile_short.write('Rollmask Issue : impossible value (not 10..01, not 110..0, not 0..011)\n')
            lst = [i for i in range(128)]
            if self.conf.yaml_opt['store_options']['skipOddCh']:
                lst = [2 * i +1 for i in range(64)]
            for ch in lst:
                stream = "channelID = " + str(63 - ch % 64) + "   "
                for sca in range(9):
                    (result_bool1, result_str1) = self.check_sca_noisy(sk, ch, sca, stream)
                    (result_bool2, result_str2) = self.check_TOA_TOT(sk, ch, stream)
                    (result_bool3, result_str3) = self.check_sca_broken(sk, ch, sca, stream)
                    if result_bool1:
                        print(result_str1)
                        break
                    elif result_bool3:
                        print(result_str3)
                        break
                    if result_bool2:
                        print(result_str2)
                        break

    def printUnusualData(self):
        self.outputfile_short = open(self.data_folder_short, 'a')
        if self.curr_ev == 0:
            self.outputfile_short.write('*********** Unusual-Data ************* \n \n')
        for sk in self.conf.yaml_opt['testswitches']['chip_array']:
            self.outputfile_short.write(
                "\n Event = " + str(self.curr_ev) + "\t Chip = " + str(sk) + "\t RollMask = " + str(
                    hex(self.rollMask[sk])) + '\n')
            if self.check_RollMask(sk) is True:
                self.outputfile_short.write('Rollmask Issue : impossible value (not 10..01, not 110..0, not 0..011)\n')
            lst = [i for i in range(128)]
            if self.conf.yaml_opt['store_options']['skipOddCh']:
                lst = [2 * i + 1 for i in range(64)]
            for ch in lst:
                stream = "channelID = " + str(63 - ch % 64) + "   "
                for sca in range(9):
                    (result_bool1, result_str1) = self.check_sca_noisy(sk, ch, sca, stream)
                    (result_bool2, result_str2) = self.check_TOA_TOT(sk, ch, stream)
                    (result_bool3, result_str3) = self.check_sca_broken(sk, ch, sca, stream)
                    if result_bool1:
                        self.outputfile_short.write(result_str1 + "\n")
                        break
                    elif result_bool3:
                        self.outputfile_short.write(result_str3 + "\n")
                        break
                    if result_bool2:
                        self.outputfile_short.write(result_str2 + "\n")
                        break
        self.outputfile_short.close()

    # for use in UnusualData only
    def check_RollMask(self, sk):  # checks rollMask for unexpected values
        if self.rollMask[sk] not in set_rm:
            return True
        else:
            return False

    def check_sca_noisy(self, sk, ch, sca, stream):  # checks sca and prints unusual values not in [min-sca, max-sca}
        tmp_stream = stream
        outside_min_and_max = ((self.data_array[sk][ch][sca] < self.conf.yaml_opt['sca_variables']['min']) or (
                self.data_array[sk][ch][sca] > self.conf.yaml_opt['sca_variables']['max']))
        if outside_min_and_max:
            for sca1 in range(15):  # print whole channel, if just one value is wrong
                tmp_stream += " " + str(self.data_array[sk][ch][sca1])
            tmp_stream += ' (noisy)'
        return outside_min_and_max, tmp_stream

    # for use in UnusualData only
    def check_sca_broken(self, sk, ch, sca, stream):  # checks sca and prints unusual values not in [min-sca, max-sca}
        tmp_stream = stream
        zero_or_four = (self.data_array[sk][ch][sca] == 0) or (self.data_array[sk][ch][sca] == 4)
        if zero_or_four:
            for sca1 in range(15):  # print whole channel, if just one value is wrong
                tmp_stream += " " + str(self.data_array[sk][ch][sca1])
            tmp_stream += ' (broken)'
        return zero_or_four, tmp_stream

    # for use in UnusualData only
    def check_TOA_TOT(self, sk, ch, stream):  # checks TOA TOT and print unusual values
        tmp_stream = stream
        At_least_one_TO_not_4 = (self.data_array[sk][ch][13] != 4) or (self.data_array[sk][ch][14] != 4)
        if At_least_one_TO_not_4:
            for sca1 in range(15):  # print whole channel, if just one value is wrong
                tmp_stream += " " + str(self.data_array[sk][ch][sca1])
            tmp_stream += ' (ToA or ToT wrong)'
        return At_least_one_TO_not_4, tmp_stream

    # #####-----The following functions work independently and perform tested ONE event------#######

    def check_full_RollMask(self):
        print('*********** RollMask-TEST *************')
        rollMask_issue = [False for sk in self.conf.yaml_opt['testswitches']['chip_array']]
        for sk in self.conf.yaml_opt['testswitches']['chip_array']:
            print("Event = " + str(self.curr_ev) + "\t Chip = " + str(sk) + "\t RollMask = " + str(
                hex(self.rollMask[sk])))
            if self.rollMask[sk] not in set_rm:
                rollMask_issue[sk] = True  # One rollMask wrong = full hexaboard not useful
                print('Rollmask Issue : impossible value (not 10..01, not 0..11..0, not 110..0, not 0..011)')
        return rollMask_issue

    def check_full_sca(self):
        print('*********** SCA-TEST *************')
        noisy_ch_counter = 0
        noisy_channels = [None for sk in self.conf.yaml_opt['testswitches']['chip_array']]
        broken_ch_counter = 0
        broken_channels = [None for sk in self.conf.yaml_opt['testswitches']['chip_array']]
        for sk in self.conf.yaml_opt['testswitches']['chip_array']:
            print("Event = " + str(self.curr_ev) + "\t Chip = " + str(sk) + "\t RollMask = " + str(
                hex(self.rollMask[sk])))
            ch_list_1 = []
            ch_list_2 = []
            lst = [i for i in range(128)]
            if self.conf.yaml_opt['store_options']['skipOddCh']:
                lst = [2 * i + 1 for i in range(64)]
            for ch in lst:
                ADC_saturated_counter = 0  # counter for 13 SCAs per ch
                noisy_sca_counter = 0
                for sca in range(9):
                    if (self.data_array[sk][ch][sca] == 0) or (self.data_array[sk][ch][sca] == 4):
                        ADC_saturated_counter += 1
                    if (ADC_saturated_counter >= self.conf.yaml_opt['sca_variables'][
                        'th_broken_per_ch']):  # all 13 ch need to be broken so that the whole channel is broken
                        broken_ch_counter += 1
                        ch_list_1.append(63 - ch % 64)
                    if sca < 9:  # test only first 9 sca for noise
                        not_in_min_max = (
                                (self.data_array[sk][ch][sca] < self.conf.yaml_opt['sca_variables']['min']) or (
                                self.data_array[sk][ch][sca] > self.conf.yaml_opt['sca_variables']['max']))
                        if not_in_min_max:
                            noisy_sca_counter += 1
                if noisy_sca_counter >= self.conf.yaml_opt['sca_variables']['th_noisy_per_ch']:
                    noisy_ch_counter += 1
                    ch_list_2.append(63 - ch % 64)
            print('broken channels:' + str(ch_list_1))
            print('noisy channels:' + str(ch_list_2))
            noisy_channels[sk] = ch_list_2
            broken_channels[sk] = ch_list_1
        return noisy_ch_counter, noisy_channels, broken_ch_counter, broken_channels

    def check_full_TOA_TOT(self):
        print('*********** ToA/ToT-TEST *************')
        wrong_To_list = [[] for sk in self.conf.yaml_opt['testswitches']['chip_array']]
        less_counts_than_threshold = [True for sk in self.conf.yaml_opt['testswitches']['chip_array']]
        for sk in self.conf.yaml_opt['testswitches']['chip_array']:
            print("Event = " + str(self.curr_ev) + "\t Chip = " + str(sk) + "\t RollMask = " + str(
                hex(self.rollMask[sk])))
            Unexpected_TO_counter = 0  # reset of unexpected_TO_counter for the next skiroc
            lst = [i for i in range(128)]
            if self.conf.yaml_opt['store_options']['skipOddCh']:
                lst = [2 * i + 1 for i in range(64)]
            for ch in lst:
                if (self.data_array[sk][ch][13] != 4) or (self.data_array[sk][ch][14] != 4):
                    Unexpected_TO_counter += 1
                    wrong_To_list[sk].append(63 - ch % 64)
                if Unexpected_TO_counter >= 1:
                    less_counts_than_threshold[sk] = False
        print('channels with ToA or ToT not 4:', wrong_To_list)
        return less_counts_than_threshold, wrong_To_list

    def printBinaryRollMask(self):  # just for debugging
        numrows = len(self.rollMask)
        numcols = len(self.rollMask[0])
        for i in range(numrows):
            stream = 'RollMask Chip' + str(i) + ': '
            for j in range(numcols):
                stream = stream + str(bin(self.rollMask[i][j])) + ' '
            print(stream)
